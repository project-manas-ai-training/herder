//
// Created by reuben on 12/1/17.
//

#ifndef PROJECT_CONTROLLER_HPP
#define PROJECT_CONTROLLER_HPP


#include <ros/ros.h>
#include <herdersim/Spawn.h>
#include <herdersim/Kill.h>
#include <geometry_msgs/Twist.h>
#include <ros/subscriber.h>
#include <herdersim/Pose.h>

#include <random>
#include <string>

using namespace ros;
using namespace std;

class Controller {
protected:
	NodeHandle &nh;
	Publisher pub;

	Subscriber sub;

	float x;

	string name;

	ServiceClient spawnClient, killClient;

protected:
	herdersim::Pose pose;
public:

	Controller(NodeHandle &, string);

	void poseCallback(const herdersim::Pose::ConstPtr &);

	void move(float, float);

	herdersim::Pose getPose();


	void spawn(herdersim::SpawnRequest req, herdersim::SpawnResponse &res);

	void kill();

	float getDist(herdersim::Pose);

	~Controller();
};


#endif //PROJECT_CONTROLLER_HPP
