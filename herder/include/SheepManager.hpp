//
// Created by reuben on 12/1/17.
//

#ifndef PROJECT_SHEEPMANAGER_HPP
#define PROJECT_SHEEPMANAGER_HPP

#include <Sheep.hpp>
#include <Dog.hpp>
#include <CornerPose.hpp>
#include <Cornered.hpp>

#include <vector>

using namespace ros;
using namespace std;

class SheepManager {
	NodeHandle &nh;
	unsigned sheepCount;
	vector<Sheep *> sheep;
	herdersim::Pose pose;
	Dog &dog;

	void poseCallback(const herdersim::Pose::ConstPtr &);

public:

	SheepManager(NodeHandle &, Dog &);

	void spawnSheep();

	void killSheep();

	void killSheep(int);

	void onGameReset();

	void onBark();

	Cornered handleSheepInCorner();

	bool isAllSheepDead();
};


#endif //PROJECT_SHEEPMANAGER_HPP
