//
// Created by reuben on 17/1/17.
//

#ifndef PROJECT_CORNER_HPP
#define PROJECT_CORNER_HPP

#include <random>

enum Corner {
	NONE,
	BOTTOM_LEFT,
	BOTTOM_RIGHT,
	TOP_LEFT,
	TOP_RIGHT
};

namespace corner {
	Corner getRandomCorner();
}

#endif //PROJECT_CORNER_HPP
