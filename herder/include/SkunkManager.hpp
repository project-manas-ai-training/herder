//
// Created by reuben on 17/1/17.
//

#ifndef PROJECT_SKUNKMANAGER_HPP
#define PROJECT_SKUNKMANAGER_HPP

#include <Skunk.hpp>
#include <Dog.hpp>

#include <vector>

using namespace ros;
using namespace std;


class SkunkManager {
	NodeHandle &nh;
	unsigned skunkCount;
	vector<Skunk *> skunk;
	herdersim::Pose pose;
	Dog &dog;

	void poseCallback(const herdersim::Pose::ConstPtr &);

public:

	SkunkManager(NodeHandle &, Dog &);

	void spawnSkunks();

	void killSkunks();

	void onGameReset();

	void moveSkunks();

	bool isDogInContact();
};


#endif //PROJECT_SKUNKMANAGER_HPP
