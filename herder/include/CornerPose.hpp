//
// Created by reuben on 17/1/17.
//

#ifndef PROJECT_CORNERPOSE_HPP
#define PROJECT_CORNERPOSE_HPP

#include "Corner.hpp"

#include <herdersim/Pose.h>

class CornerPose {
	static herdersim::Pose bottomLeft, bottomRight, topLeft, topRight;
public:
	CornerPose();

	static herdersim::Pose getCornerPose(Corner corner);
};


#endif //PROJECT_CORNERPOSE_HPP
