//
// Created by reuben on 12/1/17.
//

#ifndef PROJECT_SLAVE_HPP
#define PROJECT_SLAVE_HPP

#include <Controller.hpp>

class Slave : public Controller {
public:
	Slave(NodeHandle &, string);
};


#endif //PROJECT_SLAVE_HPP
