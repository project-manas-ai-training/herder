//
// Created by reuben on 17/1/17.
//

#ifndef PROJECT_SKUNK_HPP
#define PROJECT_SKUNK_HPP

#include <Slave.hpp>
#include <CornerPose.hpp>

using namespace ros;

class Skunk : public Slave {
public:
	Skunk(NodeHandle &n, string name);

	void spawn(herdersim::SpawnRequest, herdersim::SpawnResponse &);
};


#endif //PROJECT_SKUNK_HPP
