//
// Created by reuben on 12/1/17.
//

#ifndef PROJECT_SHEEP_HPP
#define PROJECT_SHEEP_HPP


#include <Slave.hpp>
#include <Corner.hpp>
#include <CornerPose.hpp>

class Sheep : public Slave {
public:
	Sheep(NodeHandle &, string, Corner);

	Corner corner;

	void spawn(herdersim::SpawnRequest, herdersim::SpawnResponse &);
};


#endif //PROJECT_SHEEP_HPP
