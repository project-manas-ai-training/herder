//
// Created by reuben on 17/1/17.
//

#ifndef PROJECT_GAMESESSION_HPP
#define PROJECT_GAMESESSION_HPP

#include <ros/ros.h>

using namespace ros;

class GameSession {
	Time startTimestamp;
	unsigned short correctHerdCount, wrongHerdCount, barks;
	double dogDistance;

public:
	GameSession();

	float getScore();

	void reset();

	void onSheepCornered(bool isCorrectCorner);

	void onBark();

	void addDogDistance(double);
};


#endif //PROJECT_GAMESESSION_HPP
