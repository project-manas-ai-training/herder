//
// Created by reuben on 17/1/17.
//

#ifndef PROJECT_CORNERED_HPP
#define PROJECT_CORNERED_HPP

#include "Corner.hpp"

class Cornered {
public:
	Corner corner;
	bool isCorrectCorner;

	Cornered(Corner, bool);
};

#endif //PROJECT_CORNERED_HPP
