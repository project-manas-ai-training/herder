//
// Created by reuben on 12/1/17.
//

#ifndef PROJECT_DOG_HPP
#define PROJECT_DOG_HPP

#include <Controller.hpp>

using namespace ros;

class Dog : public Controller {
public:
	Dog(NodeHandle &);

	void spawn();

	void onGameReset();
};


#endif //PROJECT_DOG_HPP
