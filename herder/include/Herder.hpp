//
// Created by reuben on 13/1/17.
//

#ifndef PROJECT_HERDER_HPP
#define PROJECT_HERDER_HPP

#include <SheepManager.hpp>
#include <SkunkManager.hpp>
#include <Dog.hpp>
#include <GameSession.hpp>

class Herder {
	NodeHandle &nh;
	SheepManager sheepManager;
	SkunkManager skunkManager;
	Dog dog;
	GameSession gameSession;

	float totalScore;
	int roundsRemaining;
public:
	Herder(NodeHandle &);

	void reset();

	void endGame();

	void onGameOver(bool);

	void updateScore();

	void bark();
};


#endif //PROJECT_HERDER_HPP
