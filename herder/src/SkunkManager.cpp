//
// Created by reuben on 17/1/17.
//

#include "../include/SkunkManager.hpp"


SkunkManager::SkunkManager(NodeHandle &n, Dog &d) : nh(n), dog(d) {
	skunkCount = (unsigned int) n.param("skunk_count", 2);
	skunk = vector<Skunk *>();
}

void SkunkManager::spawnSkunks() {
	ROS_DEBUG("Spawning %d skunk", skunkCount);
	herdersim::SpawnRequest req;
	herdersim::SpawnResponse res;
	for (unsigned i = 0; i < skunkCount; i++) {
		req.x = (float) (0.5 + random() % 10);
		req.y = (float) (0.5 + random() % 10);
		req.theta = 0;
		req.name = "skunk" + std::to_string(i);
		Skunk *newSkunk = new Skunk(nh, req.name);
		skunk.push_back(newSkunk);
		skunk.back()->spawn(req, res);
	}
}

void SkunkManager::killSkunks() {
	ROS_DEBUG("Killing %lu skunk", skunk.size());
	for (int i = (int) skunk.size() - 1; i >= 0; i--) {
		skunk.back()->Slave::kill();
		delete skunk.back();
		skunk.pop_back();
	}
}

void SkunkManager::onGameReset() {
	killSkunks();
	spawnSkunks();
}

void SkunkManager::moveSkunks() {
	for (unsigned i = 0; i < skunkCount; i++) {
		herdersim::Pose dogPose = dog.getPose();
		herdersim::Pose skunkPose = skunk[i]->getPose();

		float dx = skunkPose.x - dogPose.x;
		float dy = skunkPose.y - dogPose.y;

		if (dx == 0 && dy == 0)
			return;

		float target;
		if (dx == 0) {
			if (dy > 0) target = (float) (M_PI / 2.0);
			else target = (float) (-M_PI / 2.0);
		} else {
			target = atan(dy / dx);
			if (dy < 0 && target < 0) target += M_PI;
			else if (dy > 0 && target > 0) target -= M_PI;
		}

		float theta = skunkPose.theta;
		float dTheta = target - theta;

		skunk[i]->move((float) (0.5), dTheta);
	}
}

bool SkunkManager::isDogInContact() {
	for (Skunk *s:skunk) {
		float d = ((Controller *) s)->getDist(dog.getPose());

		if (d == 0) {
			return false;
		}
		if (d < 0.8) {
			return true;
		}
	}
	return false;
}