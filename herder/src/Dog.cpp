//
// Created by reuben on 12/1/17.
//

#include "Dog.hpp"

Dog::Dog(NodeHandle &n) : Controller(n, "dog") {
	pose.x = pose.y = 5;
}

void Dog::spawn() {
	herdersim::SpawnRequest req;
	herdersim::SpawnResponse res;

	req.x = (float) (5);
	req.y = (float) (5);
	req.theta = 0;
	req.name = "dog";
	req.imageId = 0;
	Controller::spawn(req, res);
}

void Dog::onGameReset() {
	kill();
	spawn();
}

/*
int main(int argc, char *argv[]) {
	init(argc, argv, "dog");
	NodeHandle n("~");

	Dog dog(n);

	spin();
}
 */