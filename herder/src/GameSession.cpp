//
// Created by reuben on 17/1/17.
//

#include "GameSession.hpp"


GameSession::GameSession() {
	reset();
}

float GameSession::getScore() {
	Duration time = Time::now() - startTimestamp;
	return (float) ((10 * correctHerdCount + 5 * wrongHerdCount) /
					(barks + 0.5 * dogDistance + 0.1 * time.toSec()));
}

void GameSession::reset() {
	startTimestamp = ros::Time::now();
	correctHerdCount = wrongHerdCount = barks = 0;
	dogDistance = 0;
}

void GameSession::onSheepCornered(bool isCorrectCorner) {
	if (isCorrectCorner)
		correctHerdCount++;
	else
		wrongHerdCount++;
}

void GameSession::onBark() {
	barks++;
}

void GameSession::addDogDistance(double dist) {
	dogDistance += dist;
}