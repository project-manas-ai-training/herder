//
// Created by reuben on 17/1/17.
//

#include "CornerPose.hpp"

herdersim::Pose CornerPose::bottomLeft = herdersim::Pose();
herdersim::Pose CornerPose::bottomRight = herdersim::Pose();
herdersim::Pose CornerPose::topLeft = herdersim::Pose();
herdersim::Pose CornerPose::topRight = herdersim::Pose();

CornerPose::CornerPose() {
	bottomLeft.x = bottomLeft.y = 0;

	bottomRight.x = 11;
	bottomRight.y = 0;

	topLeft.x = 0;
	topLeft.y = 11;

	topRight.x = 11;
	topRight.y = 11;
}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "OCUnusedGlobalDeclarationInspection"
static CornerPose cornerPose;
#pragma clang diagnostic pop

herdersim::Pose CornerPose::getCornerPose(Corner corner) {
	switch (corner) {
		case Corner::BOTTOM_LEFT:
			return bottomLeft;
		case Corner::BOTTOM_RIGHT:
			return bottomRight;
		case Corner::TOP_LEFT:
			return topLeft;
		case Corner::TOP_RIGHT:
			return topRight;
	}
}