//
// Created by reuben on 17/1/17.
//

#include "Skunk.hpp"

Skunk::Skunk(NodeHandle &n, string name) : Slave(n, name) {
	pose = CornerPose::getCornerPose(Corner::TOP_LEFT);
	pose.y++;
}

void Skunk::spawn(herdersim::SpawnRequest req, herdersim::SpawnResponse &res) {
	req.imageId = 1;
	Controller::spawn(req, res);
}