//
// Created by reuben on 13/1/17.
//

#include "Herder.hpp"

Herder::Herder(NodeHandle &n) : nh(n),
								dog(Dog(n)),
								sheepManager(SheepManager(n, dog)),
								skunkManager(SkunkManager(n, dog)),
								totalScore(0) {
	dog.spawn();
	sheepManager.spawnSheep();
	skunkManager.spawnSkunks();

	roundsRemaining = (unsigned int) n.param("roundsRemaining", 10);

	Rate rate(50);
	int barkInterval = 100;
	while (ros::ok() && roundsRemaining) {
		skunkManager.moveSkunks();

		if (barkInterval < 0) {
			barkInterval = 100;
			bark();
		}
		barkInterval--;

		if (skunkManager.isDogInContact()) {
			onGameOver(false);
		}

		Cornered cornered = sheepManager.handleSheepInCorner();
		if (cornered.corner != NONE) {
			if (cornered.isCorrectCorner) {
				ROS_INFO("Sheep cornered correctly!");
			} else {
				ROS_INFO("Sheep cornered incorrectly!");
			}
			gameSession.onSheepCornered(cornered.isCorrectCorner);
			if (sheepManager.isAllSheepDead()) {
				onGameOver(true);
			}
		}
		rate.sleep();

		herdersim::Pose prevPose = dog.getPose();

		spinOnce();

		double dist = dog.getDist(prevPose);
		gameSession.addDogDistance(dist);

	}
	if (ros::ok())
		endGame();
}

void Herder::reset() {
	sheepManager.onGameReset();
	skunkManager.onGameReset();
	dog.onGameReset();
	gameSession.reset();
}

void Herder::endGame() {
	sheepManager.killSheep();
	skunkManager.killSkunks();
	dog.kill();
	ROS_INFO("Game completed with final total Score: %f", totalScore);
}

void Herder::updateScore() {
	totalScore += gameSession.getScore();
	ROS_INFO("Total Score: %f", totalScore);
}

void Herder::bark() {
	sheepManager.onBark();
	gameSession.onBark();
	ROS_INFO("Dog barks");
}

void Herder::onGameOver(bool win) {
	if (win) {
		ROS_WARN("Round won!!");
	} else {
		ROS_WARN("Dog is skunk-ified!");
		updateScore();
	}
	reset();
	roundsRemaining--;
	ROS_WARN("%d rounds remaining", roundsRemaining);
}
