//
// Created by reuben on 12/1/17.
//

#include "Sheep.hpp"

Sheep::Sheep(NodeHandle &n, string name, Corner corner) : Slave(n, name) {
	pose = CornerPose::getCornerPose(Corner::BOTTOM_RIGHT);
	pose.x++;
	this->corner = corner;
}

void Sheep::spawn(herdersim::SpawnRequest req, herdersim::SpawnResponse &res) {
	switch (corner) {
		case BOTTOM_LEFT:
			req.imageId = 2;
			break;
		case BOTTOM_RIGHT:
			req.imageId = 3;
			break;
		case TOP_LEFT:
			req.imageId = 4;
			break;
		case TOP_RIGHT:
			req.imageId = 5;
			break;
		case NONE:
			req.imageId = 6;
	}
	Controller::spawn(req, res);
}