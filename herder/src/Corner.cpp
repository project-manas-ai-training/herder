//
// Created by reuben on 17/1/17.
//

#include <Corner.hpp>

namespace corner {
	Corner getRandomCorner() {
		switch (rand() % 4) {
			case 0:
				return BOTTOM_LEFT;
			case 2:
				return BOTTOM_RIGHT;
			case 1:
				return TOP_LEFT;
			case 3:
				return TOP_RIGHT;
		}
	}
}
