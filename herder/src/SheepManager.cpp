//
// Created by reuben on 12/1/17.
//

#include "SheepManager.hpp"


SheepManager::SheepManager(NodeHandle &n, Dog &d) : nh(n), dog(d) {
	sheepCount = (unsigned int) n.param("sheep_count", 5);
	sheep = vector<Sheep *>();
}

void SheepManager::spawnSheep() {
	ROS_DEBUG("Spawning %d sheep", sheepCount);
	herdersim::SpawnRequest req;
	herdersim::SpawnResponse res;
	for (unsigned i = 0; i < sheepCount; i++) {
		req.x = (float) (0.5 + random() % 10);
		req.y = (float) (0.5 + random() % 10);
		req.theta = 0;
		req.name = "sheep" + std::to_string(i);
		Sheep *newSheep = new Sheep(nh, req.name, corner::getRandomCorner());
		sheep.push_back(newSheep);
		sheep.back()->spawn(req, res);
	}
}

void SheepManager::killSheep() {
	ROS_DEBUG("Killing %lu sheep", sheep.size());
	for (int i = (int) sheep.size() - 1; i >= 0; i--) {
		sheep.back()->Slave::kill();
		delete sheep.back();
		sheep.pop_back();
	}
}

void SheepManager::killSheep(int i) {
	sheep[i]->kill();
	delete sheep[i];
	sheep.erase(sheep.begin() + i);
}

void SheepManager::onGameReset() {
	killSheep();
	spawnSheep();
}

void SheepManager::onBark() {
	for (unsigned i = 0; i < sheep.size(); i++) {
		herdersim::Pose dogPose = dog.getPose();
		herdersim::Pose sheepPose = sheep[i]->getPose();

		float dx = sheepPose.x - dogPose.x;
		float dy = sheepPose.y - dogPose.y;

		if (dx == 0 && dy == 0)
			return;

		float target;
		if (dx == 0) {
			if (dy > 0) target = (float) (M_PI / 2.0);
			else target = (float) (-M_PI / 2.0);
		} else {
			target = atan(dy / dx);
			if (dy > 0 && target < 0) target += M_PI;
			else if (dy < 0 && target > 0) target -= M_PI;
		}

		float theta = sheepPose.theta;
		float dTheta = target - theta;

		sheep[i]->move((float) (1.0 / sqrt(dx * dx + dy * dy)), dTheta);
	}
}

Cornered SheepManager::handleSheepInCorner() {
	for (int i = 0; i < sheep.size(); i++) {
		for (Corner corner:{BOTTOM_LEFT, BOTTOM_RIGHT, TOP_LEFT, TOP_RIGHT})
			if (sheep[i]->Controller::getDist(CornerPose::getCornerPose(corner)) < 0.5) {
				bool correctCorner = sheep[i]->corner == corner;
				killSheep(i);
				return Cornered(corner, correctCorner);
			}
	}
	return Cornered(NONE, false);
}

bool SheepManager::isAllSheepDead() {
	return sheep.size() == 0;
}