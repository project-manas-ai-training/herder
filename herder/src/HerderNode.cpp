//
// Created by reuben on 13/1/17.
//

#include <Herder.hpp>

int main(int argc, char *argv[]) {
	init(argc, argv, "herder_node");
	NodeHandle n;

	Herder herder(n);

	spin();
}
