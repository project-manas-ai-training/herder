//
// Created by reuben on 12/1/17.
//

#include "Controller.hpp"


Controller::Controller(NodeHandle &n, string turtleName) : nh(n) {
	name = string(turtleName);
	pose.x = pose.y = -1;

	pub = n.advertise<geometry_msgs::Twist>(string("/" + name + "/cmd_vel"), 5);

	sub = n.subscribe<herdersim::Pose>("/" + name + "/pose", 5,
									   boost::bind(&Controller::poseCallback, this, _1));

	spawnClient = n.serviceClient<herdersim::Spawn>("/spawn");
	killClient = n.serviceClient<herdersim::Kill>("/kill");

}

void Controller::move(float dx, float dTheta) {
	geometry_msgs::Twist twist;
	twist.linear.x = dx;
	twist.angular.z = dTheta;
	pub.publish(twist);
}

herdersim::Pose Controller::getPose() {
	return pose;
}

void Controller::poseCallback(const herdersim::Pose::ConstPtr &newPose) {
	pose.x = newPose->x;
	x = newPose->x;
	pose.y = newPose->y;
	pose.theta = newPose->theta;
}

void Controller::spawn(herdersim::SpawnRequest req, herdersim::SpawnResponse &res) {
	spawnClient.call(req, res);
}

void Controller::kill() {
	herdersim::KillRequest req;
	herdersim::KillResponse res;
	req.name = name;
	killClient.call(req, res);
}

float Controller::getDist(herdersim::Pose p) {
	float dx = pose.x - p.x;
	float dy = pose.y - p.y;
	return sqrt(dx * dx + dy * dy);
}

Controller::~Controller() {
}